package pt.se_ulusofona.calculadoramvc.model;


import java.io.Serializable;


public class OperacaoUnaria implements Serializable{

    private String operacao;
    private String letra;
    private String result;

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) { this.letra = letra; }


    public void executaOperacao() {
        switch (getOperacao()){
            case "inverte":
                result = new StringBuffer(letra).reverse().toString();
                break;
            case "invert":
                result = new StringBuffer(letra).reverse().toString();
                break;
            case "duplicate":
                result = new String(new char[2]).replace("\0", letra);
                break;
            case "duplica":
                result = new String(new char[2]).replace("\0", letra);
                break;
            default:
                break;
        }
    }


    public String getResult(){

        setLetra(result);
        return result;
    }
}
