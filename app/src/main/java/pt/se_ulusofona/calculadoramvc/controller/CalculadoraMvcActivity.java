package pt.se_ulusofona.calculadoramvc.controller;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import pt.se_ulusofona.calculadoramvc.model.OperacaoUnaria;
import pt.se_ulusofona.calculadoramvc.R;


public class CalculadoraMvcActivity extends AppCompatActivity {

    private final String TAG = "TAG";
    private EditText editTextField;
    private OperacaoUnaria operacaoUnariaModel;
    private boolean isUserIsInTheMiddleOfTyping=false;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calc_layout);
        operacaoUnariaModel = new OperacaoUnaria();
        editTextField = (EditText) findViewById(R.id.edit_field);
        context = this;


    }




    public void performOperation(View view) {

        Button btOperacao = (Button) view;
        operacaoUnariaModel.setOperacao(btOperacao.getText().toString());
        operacaoUnariaModel.executaOperacao();
        setDisplayValue(operacaoUnariaModel.getResult());
        isUserIsInTheMiddleOfTyping = false;
        Log.d(TAG, "performOperation: called() " + "letra: " + operacaoUnariaModel.getLetra() + " operacao " + operacaoUnariaModel.getOperacao());


    }

    public void touchDigit(View view) {

        Button btPressed = (Button) view;
        String digito = btPressed.getText().toString();
        if (isUserIsInTheMiddleOfTyping) {
            editTextField.append(digito);
        }else {
            editTextField.setText(digito);
            isUserIsInTheMiddleOfTyping = true;
        }
        operacaoUnariaModel.setLetra(getDisplayValue());

        Intent intent = null;
        switch (view.getId()) {
            case R.id.btn_som:
                intent = new Intent(context, SomaMVC.class);
                break;
            default:
                break;
        }
        if(intent != null) {
            startActivity(intent);
        }


    }

    public void clearScreen(View view) {
        editTextField.setText("X");
        isUserIsInTheMiddleOfTyping=false;
    }

    private String getDisplayValue() {
        Log.d(TAG, "getDisplayValue " + editTextField.getText().toString());
        return editTextField.getText().toString();
    }

    private void setDisplayValue(String value) {
        editTextField.setText(String.valueOf(value));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("model",operacaoUnariaModel);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        operacaoUnariaModel = (OperacaoUnaria) savedInstanceState.getSerializable("model");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calc_menu, menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.share_action:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"diogmthenriques@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Valores");
                i.putExtra(Intent.EXTRA_TEXT   , editTextField.getText().toString());
                startActivity(Intent.createChooser(i, "Send email..."));
        }


        return super.onOptionsItemSelected(item);
    }
}
