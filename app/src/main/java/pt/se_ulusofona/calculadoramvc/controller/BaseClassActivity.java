package pt.se_ulusofona.calculadoramvc.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import pt.se_ulusofona.calculadoramvc.R;


public class BaseClassActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvCalc;
    private Context context;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);

        context = this;

        tvCalc = (TextView) findViewById(R.id.tv_calc);
        tvCalc.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.tv_calc:
                intent = new Intent(context, CalculadoraMvcActivity.class);
                break;
            default:
                break;
        }
        if(intent != null) {
            startActivity(intent);
        }
    }




}
